<?php 

require_once("dbconfig.php");

$nume = "";
$prenume = "";
$mail = "";
$tel = "";
$dece = "";
$stud = "";
$facultate = "";
$error = 0;
$error_text = "";

if(isset($_POST['nume'])){
    $nume = $_POST['nume'];
}

if(isset($_POST['prenume'])){
    $prenume = $_POST['prenume'];
}

if(isset($_POST['mail'])){
    $mail = $_POST['mail'];
}

if(isset($_POST['tel'])){
    $tel = $_POST['tel'];
}
 
if(isset($_POST['dece'])){
    $dece = $_POST['dece'];
}

if(isset($_POST['stud'])){
    $stud = $_POST['stud'];
}

if(isset($_POST['facultate'])){
    $facultate = $_POST['facultate'];
}


if(empty($nume) || empty($prenume) || empty($tel) || empty($mail) || empty($dece)){
    $error = 1;
    $error_text = "One or more fields are empty!";
}

if(strlen($nume) < 3 || strlen($nume) > 20  || strlen($prenume) < 3 || strlen($prenume) > 20 || strlen($dece) < 15){
    $error = 1;
    $error_text = "First name, Last name or Question is shorter than expected!";
}

if (!preg_match("/^[a-zA-Z ]*$/",$nume)){
    $error = 1;
    $error_text = "First name must contain only letters ";
}

if (!preg_match("/^[a-zA-Z ]*$/",$prenume)){
    $error = 1;
    $error_text = "Last name must contain only letters ";
}

if(!is_numeric($tel) || strlen($tel)!=10){
    $error = 1;
    $error_text = "Phone number is not valid";
}

if (!filter_var($mail, FILTER_VALIDATE_EMAIL)){
    $error = 1;
    $error_text = "Email is not valid";
}

try {

    $con = new pdo('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8;', USER, PASSWORD);

} catch(Exception $e) {

    $db_error['connection'] = "Cannot connect to database";

    $response = json_encode($db_error);

    header("HTTP/1.1 503 Service Unavailable");

        echo $response;
    return;

}

if($error == 0) {

    $stmt2 = $con -> prepare("INSERT INTO formular(nume,prenume,mail,tel,dece,stud,facultate) VALUES(:nume,:prenume,:mail,:tel,:dece,:stud,:facultate)");

    $stmt2 -> bindParam(':nume',$nume);
    $stmt2 -> bindParam(':prenume',$prenume);
    $stmt2 -> bindParam(':mail',$mail);
    $stmt2 -> bindParam(':tel',$tel);
    $stmt2 -> bindParam(':dece',$dece);
    $stmt2 -> bindParam(':stud',$stud);
    $stmt2 -> bindParam(':facultate',$facultate);

    if(!$stmt2 -> execute()){

    	$error['connection'] = "Database Error";

    }else
        echo "Succes";
}
else{
    echo $error_text;
}

