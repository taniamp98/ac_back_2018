<?php 

require_once("dbconfig.php");

$firstname = "";
$lastname = "";
$phone = "";
$email = "";
$cnp = "";
$sex = "";
$facebook = "";
$birth = "";
$department = "";
$question = "";
$captcha_generated = "";
$captcha_inserted = "";
$check = "";
$facultate = "";
$error = 0;
$error_text = "";

if(isset($_POST['firstname'])){
	$firstname = $_POST['firstname'];
}

if(isset($_POST['lastname'])){
	$lastname = $_POST['lastname'];
}

if(isset($_POST['phone'])){
	$phone = $_POST['phone'];
}

if(isset($_POST['email'])){
	$email = $_POST['email'];
}

if(isset($_POST['cnp'])){
	$cnp = $_POST['cnp'];
}

if(isset($_POST['facebook'])){
	$facebook = $_POST['facebook'];
}

if(isset($_POST['birth'])){
	$birth = $_POST['birth'];
}

if(isset($_POST['department'])){
	$department = $_POST['department'];
}

if(isset($_POST['question'])){
	$question = $_POST['question'];
}

if(isset($_POST['captcha_generated'])){
	$captcha_generated = $_POST['captcha_generated'];
}

if(isset($_POST['captcha_inserted'])){
	$captcha_inserted = $_POST['captcha_inserted'];
}

if(isset($_POST['check'])){
	$check = $_POST['check'];
}
if(isset($_POST['facultate'])){
	$facultate = $_POST['facultate'];
}

/*VERIFICARE CAMPURI GOALE*/

if(empty($firstname) || empty($lastname) || empty($phone) || empty($email) || empty($facebook) || empty($birth) || empty($department) || empty($question) || empty($captcha_inserted) || empty($captcha_generated) || empty($check) || empty($facultate)){
	$error = 1;
	$error_text = "One or more fields are empty!";
}

/*CAMPURI CU LIMITA DE CARACTERE*/

if(strlen($firstname) < 3 || strlen($firstname) > 20  || strlen($lastname) < 3 || strlen($lastname) > 20 || strlen($question) < 15){
	$error = 1;
	$error_text = "First name, Last name or Question is shorter than expected!";
}

/*CAMPURI CE PERMIT DOAR LITERE*/

if (!preg_match("/^[a-zA-Z ]*$/",$firstname)){
	$error = 1;
	$error_text = "First name must contain only letters ";
}
if (!preg_match("/^[a-zA-Z ]*$/",$lastname)){
	$error = 1;
	$error_text = "Last name must contain only letters ";
}

/*VALIDARE NUMAR DE TELEFON*/

if(!is_numeric($phone) || strlen($phone) != 10){
	$error = 1;
	$error_text = "Phone number is not valid";
}

/*VALIDARE EMAIL*/

if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
	$error = 1;
	$error_text = "Email is not valid";
}

/*VALIDARE CNP*/

if(strlen($cnp) != 13 || $cnp[0] == 7 || $cnp[0] == 8 || $cnp[0] == 9 || $cnp[0] == 0){
	$error = 1;
	$error_text = "CNP is not valid";
}

/*VALIDARE LINK FACEBOOK*/

if(!preg_match('^facebook\.com\S*[a-zA-Z0-9_]*^', $facebook)){
	$error = 1;
	$error_text = "Facebook link is not valid";
}

/*VALIDAREA DATEI DE NASTERE*/

$date1 = new DateTime($birth);
$date2 = new DateTime();
$interval = $date1->diff($date2);
$myage = $interval->y; 
if($myage < 18 || $myage > 100){ 
	$error = 1;
    $error_text = "You must be between 18 and 100 years old";
}

/*VALIDARE CAMP CAPTCHA*/

if($captcha_inserted != $captcha_generated){
	$error = 1;
	$error_text = "Captcha is not valid";
}

/*ADAUGARE CAMP FACULTATE + VALIDARE*/

if(strlen($facultate) < 3 || strlen($facultate) > 30){
	$error = 1;
	$error_text = "University name is shorter than expected";
}
if (!preg_match("/^[a-zA-Z ]*$/",$facultate)){
	$error = 1;
	$error_text = "University name must contain only letters ";
}

/*ADAUGARE CAMP SEX*/

if($cnp[0] == 1 || $cnp[0] == 3 || $cnp[0] == 5) $sex='M';
else $sex='F';

/*VERIFICARE DATA NASTERE SI CNP*/

$birthYear = substr($birth, 2, 2);
$birthMonth = substr($birth, 5, 2);
$birthDay = substr($birth, 8, 2);
$cnpYear = substr($cnp, 1, 2);
$cnpMonth = substr($cnp, 3, 2);
$cnpDay = substr($cnp, 5, 2);
if($birthYear != $cnpYear || $birthMonth != $cnpMonth || $birthDay != $cnpDay){
	$error = 1;
	$error_text = "CNP and Birth Date do not match!";
}

try {

    $con = new pdo('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8;', USER, PASSWORD);

} catch(Exception $e) {

    $db_error['connection'] = "Cannot connect to database";

    $response = json_encode($db_error);

    header("HTTP/1.1 503 Service Unavailable");

        echo $response;
    return;

}

if($error == 0){

/*OPRIREA INREGISTRARILOR DUPA 50*/

	$stmtCount = $con -> prepare("SELECT count(*) FROM register2;");
	if(!$stmtCount -> execute()){

		$error_text = "Database Error";
		echo $error_text;

	}else{

		$count = $stmtCount->fetchColumn();

		if($count > 50){

			$error_text = "Prea multi utilizatori inregistrati.";
			echo $error_text;

		}else{

			$stmt2 = $con -> prepare("INSERT INTO register2(firstname,lastname,phone,email,cnp,sex,facebook,birth,department,question,facultate) VALUES(:firstname,:lastname,:phone,:email,:cnp,:sex,:facebook,:birth,:department,:question,:facultate)");

			$stmt2 -> bindParam(':firstname',$firstname);
			$stmt2 -> bindParam(':lastname',$lastname);
			$stmt2 -> bindParam(':phone',$phone);
			$stmt2 -> bindParam(':email',$email);
			$stmt2 -> bindParam(':cnp',$cnp);
			$stmt2 -> bindParam(':sex',$sex);
			$stmt2 -> bindParam(':facebook',$facebook);
			$stmt2 -> bindParam(':birth',$birth);
			$stmt2 -> bindParam(':department',$department);
			$stmt2 -> bindParam(':question',$question);
			$stmt2 -> bindParam(':facultate',$facultate);

			if(!$stmt2 -> execute()){
				$error_text = "Database Error";
				echo $error_text;
			}else{
				echo "Succes";
			}
		}
	}
}

else{
    echo $error_text;
}

