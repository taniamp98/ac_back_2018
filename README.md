

# AC_backend

În primul și primul rând am vrea să începem prin a vă mulțumi pentru faptul că sunteți astăzi aici și să vă felicităm pentru toată munca pe care ați depus-o până acum în cadrul întregului proces de recrutare. **Well done!**

![Clap](https://media1.tenor.com/images/df791039ee4904daf7740bbf22e23164/tenor.gif)

Acum, pentru ceea ce urmează, vă îndemnăm să vă încărcați cu și mai multă energie și bună dispoziție și vă promitem că fiecare moment și fiecare minut dedicat **SiSC**-ului va merita din plin.


**Și pentru că suntem IT și ne place eficiența, să ne focusăm asupra acestui aspect acum.**



La prezentare vă spuneam noi că în spatele muncii noastre stă întotdeauna puțină ***magie***. Azi ne dorim să vă împărtășim și vouă câteva dintre ***vrăjile*** noastre și împreună să reușim să realizăm lucruri de care niciunul dintre noi nu credea că va fi capabil. 

![Power](https://media.giphy.com/media/10hiEO7bR1G8PC/giphy.gif)

Pentru început, după cum probabil ați realizat, **Git** este ca un tărâm magic pentru noi. Aici ajung toate proiectele noastre și de aici pleacă mai departe toate creațiile către publicul larg = **muggles**. 

Pentru că ne oferă posibilitatea să menținem și lucrăm întotdeauna pe ultima versiune a fișierelor, acesta aduce un avantaj enorm unei **echipe** care colaborează constant, deci o să vă rugăm și pe voi să îi acordați la fel de multă importanță și să fiți atenți în momentul în care vom explica funcționalitățile de bază.

![Focus](https://data.whicdn.com/images/307109920/original.gif)

Puțin mai jos vom lista și **necesarele + materialele** pe care vi le-am pus la dispoziție acum câteva zile, în cazul în care va fi nevoie să le accesați ceva mai rapid:

-   [PHP](https://www.sololearn.com/Course/PHP/)  
-   [HTML](https://www.codecademy.com/learn/learn-html) 
-   XAMPP
	-  [De aici îl descărcați](https://www.apachefriends.org/ro/index.html)   
	-   [Iar aici aveți tutorial pentru install](http://www.qoncious.com/questions/how-install-xampp-windows-7)   
		-   Precizare: să nu îl instalați în **C:\Program Files**

![Sirius](https://media.giphy.com/media/H5qLZPVYSZZug/source.gif)


# Acum, să trecem la treabă!


### Ce veți avea de făcut mai exact?

Prima dată va trebui să facem rost de materialele cu care vom lucra. Pentru a face asta deschidem:
- **Linia de comandă** - dacă avem Ubuntu
- **[Git bash](https://git-scm.com/download/win)** - dacă avem Windows

Mergem în folderul **xampp/htdocs**, folosind comanda:

	cd FOLDER_PATH

De exemplu, dacă avem XAMPP instalat în directorul **(D:)**, va arăta cam așa:
	
	cd D:/xampp/htdocs


Aici vom face practic **o clonă** la tot ceea ce v-am pregătit, folosind comanda:

	git clone REPO_HTTPS_ADDRESS
	
În cazul nostru:

	git clone https://gitlab.com/itsisc/ac_backend.git


Acum, dacă navigăm în folderul de mai sus...

![Ron](https://i.giphy.com/media/rKzxE0Q5EQKFq/giphy.webp)

...vom găsi un sub-folder nou, cu toate fișierele din acest **repository**.

Mai mult sau mai puțin, acolo se regăsesc **2 formulare**, iar pentru fiecare vor exista anumite **cerințe**, de care va trebui să ne ocupăm astăzi:

register1
---
Acest formular este puțin mai special, în sensul că puterea lui **VoldERROR** l-a afectat destul de puternic și nu am reușit nici acum să îi dăm de cap...
Pentru a-l pune din nou pe picioare vom avea nevoie de ajutorul vostru.

### Ce veți avea de făcut?
-  Importați fișierul **ac.sql** într-un sistem de gestiune a bazelor de date (**phpMyAdmin**) - nu vă speriați, e extrem de ușor și vă vom explica noi pas cu pas cum să faceți asta ;)
- Rulați modulele **Apache** și **MySQL** din XAMPP Control Panel
	- În cazul în care și Apache a fost și el afectat de forțele lui Voldy și primim o eroare legată de portul **80**, vom rula comanda:
	
			net stop was /y

- Deschideți **submit.php**, analizați codul și rezolvați erorile ce apar în formular astfel încât acesta să introducă date în baza de date ***ac***, tabela ***register***.
> Pentru testare încercați să aveți deschise în paralel formularul din browser și fișierul php, iar de fiecare dată când faceți modificări să îl reîncărcați.

> În plus, pentru a nu pierde timpul, puteți prioritiza funcționalitatea, nu validitatea datelor (ex. adresa **ion@aaa.com** e la fel de bună precum **ionut.popescu@gmail.com** sau **0711222333** e la fel de ok ca **numărul vostru real de telefon**).
 
 - După ce formularul devine funcțional introduceți **5 înregistrări** în baza de date.

### Ai îndeplinit cu succes toți pașii?  
![Five](https://thumbs.gfycat.com/ActualBelatedAmbushbug-small.gif)

**Felicitări!** Atunci hai să îi anunțăm și pe ceilalți vrăjitori că problema a fost rezolvată și să urcăm codul sursă undeva...
Evident, pe Git.

- Mergem pe profilul nostru de **Gitlab** și dăm click pe butonul mare și verde cu **New Project**. 
	- Numele proiectului va fi **ac_back_2018**
	- Nivelul de vizibilitate va fi **Public**
	- Și vom bifa căsuța pentru **README.md**, pentru a-l clona mai ușor pe local
	- *Opțional, puteți adăuga o descriere  scurtă și amuzantă

-	După ce am creat proiectul o să îl clonăm în același folder **htdocs**, așa cum am învățat mai devreme
-	Ne copiem fișierele din **ac_backend** în **ac_back_2018**
-	Mergem înapoi în linia de comandă și începem să ne distrăm
> Important de menționat că ori de câte ori veți face modificări și veți vrea să le urcați pe Git va trebui să urmați aceiași pași. Deci atenție, pentru că nu e nimic dificil și merită din plin.

- Pentru a verifica dacă există diferențe între repository și folderul local rulăm:

		git status 
- Pentru a adăuga un fișier modificat din listă avem:

		git add FILE_NAME
		
- În caz că vrem să le adăugăm pe **toate**:
	
		git add .
- Acum putem rula din nou **git status**, să vedem ce fișiere au fost adăugate cu succes.
- Iar pentru ca toate modificările să aibă sens și toți ceilalți colaboratori să înțeleagă care a fost scopul update-ului, trebuie să oferim și un mesaj specific:

		git commit -m "YOUR_MESSAGE_HERE"

		ex. -->  git commit -m "Fixed VoldERRORS"
- În final vom folosi o vrajă foarte puternică, ce va trimite toată informația către proiectul pe care tocmai ce l-ați creat. Sună cam așa:

		git push

![push](https://i.giphy.com/media/7xp6u50T0Dula/giphy.webp) 

Acum că avem totul pregătit, puteți lucra mai departe în oricare dintre cele 2 foldere existente în htdocs. Recomandarea noastră ar fi **ac_back_2018**, întrucât veți putea face mai ușor commit la modificările ulterioare.

register2
---
Ei bine, aici nu este nimic stricat, dar asta nu înseamnă că toate lucrurile sunt la locul lor. Acestui formular îi lipsesc **validările**, ceea ce înseamnă că este expus magiei negre și orice vrajă îl poate pune cu ușurință la pământ. 
De data aceasta avem nevoie de voi să ne ajutați să îl **securizăm**, așadar...

### a.) Aplicați formularului următoarele validări:

1. câmpuri non-empty: firstname, lastname, phone, email, facebook, birth, department, question, captcha, check;
2. câmpuri cu limită de caractere: firstname, lastname (minim 3 caractere, maxim 20), question  (minim 15 caractere);
3. câmpuri ce permit doar litere: firstname, lastname;
4. validare pentru numărul de telefon ( lungime = 10, doar numere);
5. validare pentru email (formă specifică exemplu@zzz.yyy);
6. validare pentru CNP (lungime = 13, prima cifra poate fi 1, 2, 3, 4, 5 sau 6);
7. validarea link-ului de facebook;
8. validarea datei de naștere (permise vârste >18 ani și <100);
9. validare pentru câmpul captcha;
 
>**Mențiune:** După ce rezolvați o cerință chemați pe cineva dintre noi să valideze rezolvarea.

**Done?**

![10p](https://media1.tenor.com/images/6f03e32f56fe93eaae38252cbf5cd063/tenor.gif)
 
 ---
Acum că suntem "protejați", să îl **dezvoltăm** puțin:

### b.) Adăugați câmpul "facultate" în formular.
- Acesta trebuie să existe atât în partea de front-end, cât și în baza de date, cu tipul VARCHAR, de 30 de caractere.
- Aplicați acestuia următoarele validări: non-empty, doar litere, lungime minimă>3, lungime maximă<30

![Hogwarts](https://i.giphy.com/media/THS0CXw1xkgow/giphy.webp)

---
 Și să fim siguri că nu vom avea "**clone**" înscrise:
 
### c.) Asigurați-vă că o persoană nu se poate înscrie de două ori, testând unicitatea câmpului email în baza de date.
![clone](https://metrouk2.files.wordpress.com/2010/09/article-1285150356860-0b4c5e06000005dc-812032_636x366.jpg?quality=80&strip=all&zoom=1&resize=540%2C310)

--- 
### d.) Adăugați câmpul "sex" în formular.
- Acesta trebuie să apară doar în baza de date și să fie creat pe baza CNP-ului.
> **Notă**: 
> Dacă prima cifră a CNP-ului este **1, 3 sau 5** în baza de date se va introduce litera "**M**", iar dacă prima cifră a CNP-ului este **2, 4 sau 6** în baza de date se va introduce litera "**F**"
> 
![Hermoine](https://media1.popsugar-assets.com/files/thumbor/VjRuN9c1uEwmOOVwoqetG_rVffM/fit-in/1024x1024/filters:format_auto-!!-:strip_icc-!!-/2015/07/22/932/n/3019466/efa787f09d73c3f6_8edc4481149fbbf3_tumblr_mlvtxe80mL1rta0cfo1_500.xxxlarge/i/You-May-Surprised-Who-You-Fall-Love.gif)

 ---
### e.) Închideți posibilitatea de înscriere a participanților dacă s-au înscris deja 50 de persoane.
 
![Wall](https://emilykager.files.wordpress.com/2014/06/9bb42-toptenthingsonmyharrypotterbucketlist7.gif?w=500&h=184)
 ---
### f.) Analizați data de naștere și CNP-ul.
- Permiteți înscrierea unui participant doar dacă aceste 2 câmpuri sunt compatibile.


___
### Dacă ai ajuns până aici cu bine, atunci FELICITĂRI...
![Harry](https://media1.tenor.com/images/0f8588a031f9aaa157df4f519b65180e/tenor.gif)

Iar pentru că un vrăjitor își ține întotdeauna codul pe **Git**...

	push push push!
